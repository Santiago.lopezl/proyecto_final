import pygame
pygame.init()
pantalla=pygame.display.set_mode((1000,500))

a=(250,250,250)
b=(0,250,0)
c=(250,0,0)
Negro=(0,0,0)
verde=(0,250,0)
x=500
y=250
ancho=50
alto=50
altob=50
radio=10
anchob=100
reloj=pygame.time.Clock()
velocidad=70
velocidad2=150
vel=0
dx=6
dy=6
x1=700
cuadros=[0,1,2,3,4,5,6,7,8]
cuadros2=[0,1,2,3,4,5,6,7,8]
cuadros3=[0,1,2,3,4,5,6,7,8]
inicio=True  
partida=True
fin=True

while inicio:
    reloj.tick(27)
    for evento in pygame.event.get():
        if evento.type==pygame.QUIT:
            quit()
            
    pantalla.fill(Negro)
    letra=pygame.font.SysFont("impact",100)
    letrains=pygame.font.SysFont("impact",20)
    titulo=letra.render("ARKANOID",True,(255,255,255))
    instrucciones=letrains.render("Utilice las teclas flecha izquierda y flecha derecha para controlar la raqueta",True,(255,0,0))
    inicio=letrains.render("PRESIONE ENTER PARA INICIAR EL JUEGO",True,(0,0,255))
    pantalla.blit(inicio,(320,200))
    pantalla.blit(instrucciones,(178,320))
    pantalla.blit(titulo,(270,5))
    
    tecla=pygame.key.get_pressed()
    
    if tecla[pygame.K_RETURN]:
        inicio=False
        partina=True

    
    pygame.display.update()

while partida:
    
    reloj.tick(velocidad)
    pantalla.fill(Negro)
    raqueta=pygame.draw.rect(pantalla,verde,(x1,480,200,10))

      
    for evento in pygame.event.get():
        if evento.type==pygame.QUIT:
            quit()
        elif evento.type==pygame.KEYDOWN:
            if evento.key==pygame.K_LEFT:
                x1=x1-velocidad2
                if x1<0:
                    x1=0
                    
            elif evento.key==pygame.K_RIGHT:
                x1=x1+velocidad2
                if x1>(1000-200):
                    x1=1000-200
                       
               
    x=x+dx
    y=y+dy
    if x>1000-radio:
        dx=dx*-1  
    if x<0:
        dx=dx*-1        
    if y>520-radio:
        partida=False
    if y<0:
        dy=dy*-1
        
    movil=pygame.draw.circle(pantalla,verde,(x,y),radio)
    
    dibujos=[]
    dibujos2=[]
    dibujos3=[]
    
    for cuadro3 in cuadros3:
        dibujos3.append(pygame.draw.rect(pantalla,b,(cuadro3*113,120,anchob,altob)))
        
    p=0
    for dibujo3 in dibujos3:
        if movil.colliderect(dibujo3):
            dx=dx*-1
            dy=dy*-1
            del(cuadros3[p])
        p=p+1
    
    for cuadro2 in cuadros2:
        dibujos2.append(pygame.draw.rect(pantalla,a,(cuadro2*113,60,anchob,altob)))
        
    h=0
    for dibujo2 in dibujos2:
        if movil.colliderect(dibujo2):
            dx=dx*-1
            dy=dy*-1
            del(cuadros2[h])
        h=h+1
    
    
    for cuadro in cuadros:
        dibujos.append(pygame.draw.rect(pantalla,c,(cuadro*113,0,anchob,altob)))
         
    i=0
    for dibujo in dibujos:
        if movil.colliderect(dibujo):
            dx=dx*-1
            dy=dy*-1
            del(cuadros[i])
        i=i+1        
    
    if movil.colliderect(raqueta): 
        dx=dx*-1
        dy=dy*-1

    pygame.display.update()
    
if partida==False:
    while fin:
        reloj.tick(27)
        for evento in pygame.event.get():
            if evento.type==pygame.QUIT:
                quit()
                
        pantalla.fill(Negro)
        letra=pygame.font.SysFont("impact",100)
        letrains=pygame.font.SysFont("impact",20)
        anuncio=letra.render("JUEGO TERMINADO",True,(255,255,255))
        instrucciones2=letrains.render("PRESIONE ENTER PARA SALIR DEL JUEGO",True,(255,0,0))
        pantalla.blit(instrucciones2,(325,320))
        pantalla.blit(anuncio,(147,5))
        tecla=pygame.key.get_pressed()
    
        if tecla[pygame.K_RETURN]:
            fin=False
            inicio=True
            
        pygame.display.update()
